<?php

/**
 * @file
 * Enables mail receipt of forum posts and comments.
 */

define("FORUMMAIL_ADMIN", "administer forummail");

/**
 * Implementation of hook_menu
 */
function forummail_menu($may_cache) {
	$items = array();

	if($may_cache){

	}

	$items[] = array('path' => 'forummail/help', 'title' => t('Help'), 'callback' => 'forummail_help_action', 'access' => user_access('access content'));
	return $items;

}

/**
 * Implementation of hook_perm
 */
function forummail_perm(){
	return array(FORUMMAIL_ADMIN);
}


/**
 * Implementation of hook_nodeapi
 */
function forummail_nodeapi(&$node, $op){
	global $base_url;
	$hash = md5($node->nid);
	$recipients = forummail_get_recipients();
	if($recipients){
		if($node->type == 'forum' || $node->type == 'comment'){
			if($node->uid > 0){
				$user = user_load(array('uid' => $node->uid));	
				$from = $user->mail;
			}
			else{
				$from = variable_get('site_mail', '');
			}

			$msg = 'URL: ' . $base_url . '/node/' . $node->nid . "\n\n";
			$msg .= $node->body;			

			switch ($op){
				case 'insert':
				case 'update':
					foreach($recipients as $recipient){
						mail($recipient, variable_get('site_name', '') . ' Forum Post: ' . $node->title . ' (' . $hash . ')', $msg, 'From: ' . $from);
					}
					break;
				default:
			}
		}
	}
}

/**
 * Implementation of hook_comment
 */
function forummail_comment($op, $comment){
	global $base_url;
	switch($op){
		case 'insert':
		case 'update':
			//print '<pre>' . print_r($comment, 1) . '</pre>';
			$node = node_load(array('nid' => $comment['nid']));
			$hash = md5($node->nid);
			$recipients = forummail_get_recipients();

			if($comment['uid'] > 0){
				$user = user_load(array('uid' => $comment['uid']));	
				$from = $user->mail;
			}
			else{
				$from = variable_get('site_mail', '');
			}

			$msg = 'URL: ' . $base_url . '/node/' . $node->nid . '#comment-' . $comment['cid'] . "\n\n";
			$msg .= $comment['comment'];

			foreach($recipients as $recipient){
				mail($recipient, variable_get('site_name', '') . ' Forum Post: ' . $node->title . ' (' . $hash . ')', $msg, 'From: ' . $from);
			}
			//print '<pre>' . print_r($node, 1) . '</pre>';
			break;
		default:
	}
}

/**
 * Processes results from help form and emails or posts to the forum as appropriate.
 */
function forummail_help_action(){
	if($_POST['edit']){
		global $user;
		$edit = $_POST['edit'];
		$status = forummail_help_validate($edit);
		//If there were no email field validation errors...
		if(sizeof($status) == 0){
			//If the user wants to post this to the forums...
			if($edit['audience'] == 'forum'){
				$node = (object) $edit;
				$node = node_prepare($node);
				$node->type = 'forum';
				$node->uid = $user->uid;
				$node->status = 1; //Probably shouldn't hard-code this.
				$node->comment = 2; //Probably shouldn't hard-code this.
				$node->tid = $node->taxonomy[0];
				$nid = node_save($node);	
				if($nid > 0){
					drupal_set_message('Your query has been emailed to our staff and posted in the forum below.');	
					drupal_goto('node/' . $nid);
				}
				else{
					print theme('page', 'Error creating forum posting.');
				}
			}
			//Else just send email.
			else{
				$hash = md5($edit['from'] . $edit['body']);
				$recipients = forummail_get_recipients();
				foreach($recipients as $recipient){
					mail($recipient, variable_get('site_name', '') . ' Help Request: ' . $edit['title'] . ' (' . $hash . ')', $edit['body'], 'From: ' . $edit['from']);
				}
				print theme('page', 'Your query has been emailed to our staff. We\'ll respond as soon as we can, but please bear in mind that volume in large dictates response time. We encourage you to take part in the community by posting queries inthe forums.');
			}
		} //End email validation check
		//Else print email validation errors.	
		else{
			$output = '';
			foreach($status as $stat){
				$output .= $stat . '<br />';
			}
			//And reprint the form; pass $edit so we can repopulate fields.
			$output .= forummail_help_form($edit);
			print theme('page', $output);
		}
	}
	//Only want this to be executed on post.
	else{
		drupal_not_found();
	}
}

/**
 * If the user has opted to send mail only rather than posting to the forum, make sure the email address specified is valid.
 */
function forummail_help_validate($edit){
	$status = array();
	if($edit['audience'] == 'mail' && !preg_match('/^[A-Za-z0-9\-_\.]{1,}@[A-Za-z0-9\-_\.]*\.[a-zA-Z]{2,4}/', $edit['from'])){
		$status[] = 'Invalid email address';
		drupal_set_message('Invalid email address.');
	}
	return $status;
}

/**
 * Print a form that can be used to submit queries either to the forums or to a support email address.
 */
function forummail_help_form($edit = array()){
	global $user;

	//Do this so we can repopulate the taxonomy form if catching validation error.
	$node = (object) $edit;

	$output .= form_textfield(t('From'), 'from', $edit['from'] ? $edit['from'] : $user->mail, 40, 40, 'Enter your email address if you desire a followup');
	$output .= form_textfield(t('Subject'), 'title', $edit['title'], 40, 40);
	$output .= form_radios(t('Audience'), 'audience', $edit['audience'] ? $edit['audience'] : 'mail', array('mail' => 'Mail Only', 'forum' => 'Forums'), 'Select "Mail Only" to send a private inquiry to our staff; else post to the forums for public review and comment. "Forums" item below is relevant only in the latter case.');
	$output .= implode('', taxonomy_node_form('forum', $node));
	$output .= form_textarea(t('Body'), 'body', $edit['body'], 60, 20, '');
	//$output .= filter_form('format', $edit['format']);
	$output .= form_submit('Submit');
	$output = form($output, 'post', 'forummail/help');
	return $output;
}

/**
 * Fetch settings for forummail recipients, and split on newlines for multiple recipients.
 */
function forummail_get_recipients(){
	$recipients = variable_get('forummail_recipients', '');
	$recipients = split("\n", $recipients);
	if(sizeof($recipients) == 0){
		return null;
	}
	return $recipients;
}

/**
 * Implementation of hook_settings
 */
function forummail_settings(){
	$output = form_textarea(t('Recipients'), 'forummail_recipients', variable_get('forummail_recipients', ''), 40, 6, 'Enter the email addresses of those who should be notified upon forum post or comment, one address per line.');
	return $output;
}


?>
