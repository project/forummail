Readme
------

This module works in conjunction with the forum module to provide a front-end
for support queries. Users can opt to send queries by mail only or to post to
the forums. If a recipient (or newline-separated list of recipients) has been
defined in the forummail settings, this/these address(es) will receive new
forum postings and comments in addition to any mail-only queries.

Note the function forummail_help_form(), which can be called in any
PHP-filtered node to print the help form. 



Requirements
------------

This module has been tested on Drupal 4.6.


Installation
------------

1. Copy forummail.module to the Drupal modules/ directory.

2. Enable forummail in the "site settings | modules" administration screen.

3. Set access control so that admin users can administer forummail.

4. Modify forummail settings by adding a recipient or newline-separated list
   of recipients for mail messages generated on mail query or forum posting.
